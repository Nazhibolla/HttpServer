package com.service.utils;

import static com.service.utils.Constants.APPLICATION_JSON;
import static com.service.utils.Constants.APPLICATION_XML;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HandleTimeZoneRequest {
	
	private String zipCode;
	private String accept;
	
    private String clientKey= "0JcmgSDA71asC4nNhSr33N1rEEYavmiX3gy9EAJjCeeV1MbAHmXAHGSSMI3ggvUT";
	
	public HandleTimeZoneRequest(String zipCode, String accept) {
		super();
		this.zipCode = zipCode;
		this.accept = accept;
	}
	
	public static void main(String[] args) {
		HandleTimeZoneRequest htzr = new HandleTimeZoneRequest("56125", APPLICATION_XML);
		String res = htzr.handleRequest();
		System.out.println(res);
	}
	
	public String handleRequest() {
		if(accept.equalsIgnoreCase(APPLICATION_XML)) {
			
		}
		String dataType = accept.equals(APPLICATION_JSON)?"info.json" : accept.contentEquals(APPLICATION_XML)?"info.xml" : null;
		URL url = null; 
		try {
			
			url = new URL("https://www.zipcodeapi.com/rest/"+clientKey+ "/"+ dataType + "/" + zipCode + "/radians");
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		if(url != null) {
			try {
				HttpURLConnection con = (HttpURLConnection) url.openConnection();
				con.setRequestMethod("GET");
				int resCode = con.getResponseCode();
				if(resCode == HttpURLConnection.HTTP_OK) {
					BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
					String inputLine;
					StringBuffer resp = new StringBuffer();
					while((inputLine = in.readLine()) != null) {
						resp.append(inputLine);
					}
					in.close();
//					System.out.println(resp.toString());
					return resp.toString();
				} else {
					System.out.println("Get request not worked");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}


}
