package com.service.utils;

import static com.service.utils.Constants.ACCESS_KEY;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONObject;
import org.json.XML;

//3a4f433967a7910c02e102591ea1727c
public class HandleWeatherRequest {
	
	private String cityCode;
	private String accept;
	
	public static void main(String[] args) {
		HandleWeatherRequest hwr = new HandleWeatherRequest("Astana", "application/xml");
		Object obj = hwr.handleRequest();
		
	}
	
	public HandleWeatherRequest(String cityCode, String accept) {
		this.cityCode = cityCode;
		this.accept = accept;
	}
	
	public String handleRequest() {
		URL url = null; 
		try {
			url = new URL("http://api.weatherstack.com/current"+ 
					"?access_key=" + ACCESS_KEY + 
					"&query="+cityCode);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		if(url != null) {
			try {
				HttpURLConnection con = (HttpURLConnection) url.openConnection();
				con.setRequestMethod("GET");
				int resCode = con.getResponseCode();
				if(resCode == HttpURLConnection.HTTP_OK) {
					BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
					String inputLine;
					StringBuffer resp = new StringBuffer();
					while((inputLine = in.readLine()) != null) {
						resp.append(inputLine);
					}
					in.close();
					System.out.println(resp.toString());
					JSONObject obj = new JSONObject(resp.toString());
					JSONObject respObj = (JSONObject) obj.get("current");
					if(accept.equals("application/xml")) {						
						String respXml = XML.toString(respObj, "Weather");
						return respXml;
					}
					return respObj.toString();
				} else {
					System.out.println("Get request not worked");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	
}
