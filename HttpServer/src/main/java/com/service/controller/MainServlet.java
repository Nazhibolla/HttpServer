package com.service.controller;

import static com.service.utils.Constants.APPLICATION_JSON;
import static com.service.utils.Constants.APPLICATION_XML;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.activemq.artemis.utils.json.JSONException;
import org.apache.activemq.artemis.utils.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.service.utils.HandleTimeZoneRequest;
import com.service.utils.HandleWeatherRequest;

/**
 * Servlet implementation class MainServlet
 */
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final int GET_ = 1;
	private static final int POST_ = 2;

	/**
	 * Default constructor. 
	 */
	public MainServlet() {
		// TODO Auto-generated constructor stub
	}

	private Integer parseInt(String text) {
		try {
			return Integer.parseInt(text);
		} catch (NumberFormatException e) {
			return null;
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		handleRequest(request, response, GET_);
	}
	
	private void handleRequest(HttpServletRequest request, HttpServletResponse response, int requestType) throws ServletException, IOException{
		if(request.getParameter("city") != null) {
			String cityCode = request.getParameter("city").trim();
			String accept = request.getParameter("accept").trim();
			if(accept != null && (accept.equals(APPLICATION_JSON) || accept.equals(APPLICATION_XML))) {
				HandleWeatherRequest hwr = new HandleWeatherRequest(cityCode, accept);
				String res = hwr.handleRequest();
				response.getWriter().append(res);
			} else {
				response.getWriter().append("Bad Request");
			}

		} else {
			if(requestType == POST_) {
				HandleTimeZoneRequest htr;
				String type = request.getContentType();
				System.out.println("content type : " + type);
				StringBuffer content = new StringBuffer();
				String line = null;
				try {
					BufferedReader reader = request.getReader();
					while ((line = reader.readLine()) != null)
						content.append(line);
				} catch (Exception e) { /*report an error*/ }
				if(type.equalsIgnoreCase(APPLICATION_JSON)) {
					String res = checkContentToJson(content.toString());
					response.getWriter().append(res);

				} else if(type.equalsIgnoreCase(APPLICATION_XML)) {					
					String res = checkContentToXml(content.toString());
					response.getWriter().append(res);
				} else {					
					String res = checkContentToXml(content.toString());
					if(res == null) {
						res = checkContentToJson(content.toString());
					}
					response.getWriter().append(res);
				}
			}
			
		}
	}
	
	private String checkContentToJson(String content) {
		try {
			JSONObject job =  new JSONObject(content.toString());
			String zipCode = (String)job.get("zip");
			String accept = APPLICATION_JSON;
			HandleTimeZoneRequest htr = new HandleTimeZoneRequest(zipCode, accept);
			return htr.handleRequest();
		} catch (JSONException e) {
			System.out.println("Content is not json");
			return null;
		}
	}
	
	private String checkContentToXml(String content) {
		try {
			Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(content.toString())));
			String zipCode = doc.getElementsByTagName("zip").item(0).getTextContent();
			String accept = APPLICATION_XML;
			HandleTimeZoneRequest htr = new HandleTimeZoneRequest(zipCode, accept);
			return htr.handleRequest();
		} catch (Exception e) {
			System.out.println("the content is not xml ");
			return null;
		}
	}

	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub		
		handleRequest(request, response, POST_);
	}

}
